# coding: utf-8

# !pip uninstall tensorflow-gpu
# !pip install tf-nightly-gpu
import os
import time
import math
import numpy as np
import tensorflow as tf
import cv2
import sys
os.environ["CUDA_VISIBLE_DEVICES"]="-1"    
print("Tensorflow: ", tf.VERSION)
tf.enable_eager_execution()
print("Opencv: ", cv2.__version__)

BUFFER_SIZE = 800 # take 800 at a time and shuffle
BATCH_SIZE = 50
IMG_WIDTH = 256
IMG_HEIGHT = 256

OUTPUT_CHANNELS = 1

# TODO: global influence

class Downsample(tf.keras.Model):
    
  def __init__(self, filters, size, apply_batchnorm=True):
    super(Downsample, self).__init__()
    self.apply_batchnorm = apply_batchnorm
    initializer = tf.random_normal_initializer(0., 0.04) # TODO: try for 0.2 stddev

    self.conv1 = tf.keras.layers.Conv2D(filters, 
                                        (size, size), 
                                        strides=2, 
                                        padding='same',
                                        kernel_initializer=initializer,
                                        use_bias=False)
    if self.apply_batchnorm:
        self.batchnorm = tf.keras.layers.BatchNormalization()
  
  def call(self, x, training):
    x = self.conv1(x)
    if self.apply_batchnorm:
        x = self.batchnorm(x, training=training)
    x = tf.nn.leaky_relu(x)
    return x 


class Upsample(tf.keras.Model):
    
  def __init__(self, filters, size, apply_dropout=False):
    super(Upsample, self).__init__()
    self.apply_dropout = apply_dropout
    initializer = tf.random_normal_initializer(0., 0.04)

    self.up_conv = tf.keras.layers.Conv2DTranspose(filters, 
                                                   (size, size), 
                                                   strides=2, 
                                                   padding='same',
                                                   kernel_initializer=initializer,
                                                   use_bias=False)
    self.batchnorm = tf.keras.layers.BatchNormalization()
    if self.apply_dropout:
        self.dropout = tf.keras.layers.Dropout(0.5)

  def call(self, x1, x2, training):
    x = self.up_conv(x1)
    x = self.batchnorm(x, training=training)
    if self.apply_dropout:
        x = self.dropout(x, training=training)
    x = tf.nn.relu(x)
    x = tf.concat([x, x2], axis=-1)
    return x


class Unet(tf.keras.Model):
    
  def __init__(self):
    super(Unet, self).__init__()
    initializer = tf.random_normal_initializer(0., 0.04)
    
    self.down1 = Downsample(64, 4, apply_batchnorm=False)
    self.down2 = Downsample(128, 4)
    self.down3 = Downsample(256, 4)
    self.down4 = Downsample(512, 4)
    self.down5 = Downsample(512, 4)
    self.down6 = Downsample(512, 4)
    self.down7 = Downsample(512, 4)
    self.down8 = Downsample(512, 4)

    self.up1 = Upsample(512, 4, apply_dropout=True)
    self.up2 = Upsample(512, 4, apply_dropout=True)
    self.up3 = Upsample(512, 4, apply_dropout=True)
    self.up4 = Upsample(512, 4)
    self.up5 = Upsample(256, 4)
    self.up6 = Upsample(128, 4)
    self.up7 = Upsample(64, 4)

    self.last = tf.keras.layers.Conv2DTranspose(OUTPUT_CHANNELS, 
                                                (4, 4), 
                                                strides=2, 
                                                padding='same',
                                                kernel_initializer=initializer)
  
  # @tf.contrib.eager.defun
  def call(self, x, training):
    # x shape == (bs, 256, 256, 3)    
    x1 = self.down1(x, training=training) # (bs, 128, 128, 64)
    x2 = self.down2(x1, training=training) # (bs, 64, 64, 128)
    x3 = self.down3(x2, training=training) # (bs, 32, 32, 256)
    x4 = self.down4(x3, training=training) # (bs, 16, 16, 512)
    x5 = self.down5(x4, training=training) # (bs, 8, 8, 512)
    x6 = self.down6(x5, training=training) # (bs, 4, 4, 512)
    x7 = self.down7(x6, training=training) # (bs, 2, 2, 512)
    x8 = self.down8(x7, training=training) # (bs, 1, 1, 512)



    x9 = self.up1(x8, x7, training=training) # (bs, 2, 2, 1024)
    x10 = self.up2(x9, x6, training=training) # (bs, 4, 4, 1024)
    x11 = self.up3(x10, x5, training=training) # (bs, 8, 8, 1024)
    x12 = self.up4(x11, x4, training=training) # (bs, 16, 16, 1024)
    x13 = self.up5(x12, x3, training=training) # (bs, 32, 32, 512)
    x14 = self.up6(x13, x2, training=training) # (bs, 64, 64, 256)
    x15 = self.up7(x14, x1, training=training) # (bs, 128, 128, 128)

    x16 = self.last(x15) # (bs, 256, 256, 1)
    x16 = tf.nn.sigmoid(x16)
    #print(np.argwhere(np.isnan(x9.numpy())))
    #print(x16)
    return x16

unet = Unet()


# for pc
# checkpoint_dir = './training_checkpoints'

# for drive
checkpoint_dir = 'TrainCheckpointsNew/'

checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
checkpoint = tf.train.Checkpoint(unet=unet)

# restoring the latest checkpoint in checkpoint_dir
print(tf.train.latest_checkpoint(checkpoint_dir))

checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))#.assert_existing_objects_matched()


def generate_image(model, test_input):
  prediction = model(tf.convert_to_tensor(test_input), training=False)
  
  # print(prediction.numpy())
  
  return prediction[0]

def make_image(orig, stride):
  orig = orig / 255

  xi, yi = orig.shape[0], orig.shape[1]
  xn, yn = xi, yi
  if xi % stride != 0:
    xn += stride - (xi % stride)
  if yi % stride != 0:
    yn += stride - (yi % stride)

  padded_o = np.full((xn, yn, 3), 1, dtype=np.float32)
  padded_o[0:xi, 0:yi, :] = orig

  new = np.zeros((xn, yn, 1))
  freq = np.zeros((xn, yn, 1))

  for i in range(0, xn, stride):
    if (i + 256 > xn):
        break
        
    for j in range(0, yn, stride):
      if (j + 256 > yn):
        break
      
      test_input = padded_o[i:i+256, j:j+256, :]
      
      out = generate_image(unet, test_input.reshape(1, 256, 256, 3))
      out = out.numpy()
      # return
      
      new[i:i+256, j:j+256, :] = new[i:i+256, j:j+256, :] + out
      freq[i:i+256, j:j+256, :] = freq[i:i+256, j:j+256, :] + np.ones_like(out)
      
  new = np.divide(new, freq)
  new = new[0:xi, 0:yi, :]

  new = np.concatenate([new, new, new], axis=2)
  
  ##############################################################
  # NOTE: Thresholding out at 0.5 to get final binarized image #
  ##############################################################
  
  new[new > 0.5] = 1
  new[new <= 0.5] = 0
  new = new.astype(int)
  new = new * 255
  
  return new

# path1 = "drive/My Drive/Colab Notebooks/Datasets/Dataset_18/Dataset/2.bmp"
# path2 = "drive/My Drive/Colab Notebooks/Outputs/Output_18/2_out.bmp"

def main(path1, path2):
  print("arg1: ", path1, "\narg2: ", path2)

  # read and convert from bgr to rgb
  img1 = cv2.imread(path1)
  img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

  # get output image by overlapping patch averaging
  out = make_image(img1, 64)

  print("Write success? ", cv2.imwrite(path2, out))

if __name__ == "__main__":
  if (len(sys.argv) != 3):
    raise RuntimeError("Exactly 2 arguments must be provided.")
  main(sys.argv[1], sys.argv[2])